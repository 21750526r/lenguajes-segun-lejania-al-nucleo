# Lenguajes segun lejania al nucleo

## Lenguajes de bajo nivel

El lenguaje de bajo nivel es el lenguaje que **más cerca está del hardware** por lo que puede resultar muy complicado para una persona.

Se divide en 2 tipos:


* El lenguaje de la máquina o binario:

El lenguaje binario es el más sencillo, y forma parte cualquier sistema informático. Solo de dos carácteres: 0 o 1. Esto se puede traducir a la electrónica en forma de tensión alta o tensión baja, encendido o apagado, abierto cerrado, etc. 
Un poco por arriba se encuentra el lenguaje de la máquina, que también consiste en 0 y 1. Y se usa para comunicarse directamente con la CPU y transmitir de forma literal órdenes o datos a ser procesados.

_Ejemplo:_

```0100100010100101001001011110110```


* El lenguaje ensamblador

Sus siglas en inglés *assembly language* y su abreviación **asm**.
Es un lenguaje de bajo nivel. Lo cual significa que la máquina tiene su propio conjunto de instrucciones para realizar diversas operaciones, como obtener entradas desde el teclado, mostrar información en la pantalla y realizar otros trabajos.
 
Este conjunto de instrucciones se denominan **instrucciones en lenguaje de máquina**, que son cadenas de unos y ceros.
 
El lenguaje assembly está diseñado para representar varias instrucciones en código simbólico de una forma más comprensible para el humano.


_Ejemplo:_

```
MOV al, 061h
ADD93
```


## Lenguajes de alto nivel

Los lenguajes de alto nivel están pensados para poder ejecutarse en cualquier máquina, independientemente del hardware que utilice.
Esto es gracias a los compiladores y intérprete. Normalmente esto ya viene incluido en sistema operativo y todo lo que haya pasado por compiladores y interprete se podrá ejecutar en cualquier sistema operativo no importa el hardware que utilice




 
Compilador y interprete cumplen con la misma función convertir el código de software que se ha escrito a un formato ejecutable
Sirve para cualquier máquina y cualquier sistema, permite la creación de programas complejos con menos líneas.


